/* eslint-disable comma-dangle */
/* eslint-disable space-before-function-paren */
/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable consistent-return */
/* eslint-disable no-multi-assign */
/* eslint-disable quotes */
import "@/bootstrap";
import Vue from "vue";
import VModal from "vue-js-modal";
import * as VueGoogleMaps from "vue2-google-maps";
import moment from "moment";
import VeeValidate from "vee-validate";
import VCalendar from 'v-calendar';
import VueQRCodeComponent from "vue-qrcode-component";
import VeeValidateLaravel from "vee-validate-laravel-extended";
import VueTheMask from "vue-the-mask";
import firebase from "firebase/app";
import App from "@/App";
import store from "@/store";
import router from "@/router";

window.$ = window.jQuery = require("jquery");

Vue.use(VeeValidate);
Vue.use(VeeValidateLaravel);
Vue.use(VCalendar);
Vue.use(VueTheMask);
Vue.use(VModal);

Vue.component("qr-code", VueQRCodeComponent);

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyD8gjcyRDMApm6Z0oDEQsZKTpTQqYYlSwM",
    libraries: "places"
  }
});

Vue.filter("formatDate", function(value) {
  if (value) {
    return moment(String(value)).format("MMMM Do, YYYY");
  }
});

// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyDTrKkhJCM4ZNbFXRTq0AE2uKzNlpo3_i4",
  authDomain: "dd-gig2gig.firebaseapp.com",
  databaseURL: "https://dd-gig2gig.firebaseio.com",
  projectId: "dd-gig2gig",
  storageBucket: "dd-gig2gig.appspot.com",
  messagingSenderId: "593196123450",
  appId: "1:593196123450:web:796a695a5e872524fc3c03"
});

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
