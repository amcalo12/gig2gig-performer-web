import BaseService from './core/BaseService';
import TokenService from './core/TokenService';

class ProfileService extends BaseService {
  async fetch() {
    return this.get(`a/users/show/${TokenService.getUserId()}`);
  }

  async update(resource) {
    return this.put(`a/users/update/${resource.id}`, resource);
  }
}

export default new ProfileService();
