import axios from 'axios';
import uuid from 'uuid/v1';
import firebase from 'firebase/app';
import * as types from '@/store/types';
import 'firebase/storage';

export default {
  async store({ commit }, { objData, file }) {
    const url = '/a/media/online';
    const fileName = `${uuid()}.${file.name}`;
    const obj = await firebase
      .storage()
      .ref(`${new Date()}/${fileName}`)
      .put(file);
    const uploadFile = await obj.ref.getDownloadURL();
    const paramData = {
      type: objData.type,
      url: uploadFile,
      name: fileName,
      appointment_id: objData.appointment_id,
    };
    commit(types.RESPONSE_FIREBASE_DATA, paramData);
    const {
      // eslint-disable-next-line no-unused-vars
      data: { data },
    } = await axios.post(url, paramData);
  },
};
