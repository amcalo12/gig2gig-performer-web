import getters from './getters';
import actions from './actions';
import mutations from './mutations';

const state = {
  firebasedata: [],
  // auditionGeneralList: []
  // searchQueryParam: '',
};

export default {
  state,
  getters,
  actions,
  // eslint-disable-next-line comma-dangle
  mutations
};
