/* eslint-disable eol-last */
import axios from 'axios';
import uuid from 'uuid/v1';
import firebase from 'firebase/app';
import * as types from '@/store/types';
import 'firebase/storage';


export default {
  async storeMarketPlace({ commit }, { objData, file }) {
    const url = 'a/marketplaces/create';
    const fileName = `${uuid()}.${file.name}`;
    const obj = await firebase
      .storage()
      .ref(`${new Date()}/${fileName}`)
      .put(file);
    const uploadFile = await obj.ref.getDownloadURL();
    const paramData = {
      title: objData.bussinessname,
      services: objData.description,
      phone_number: objData.phone,
      email: objData.email,
      address: objData.address,
      url_web: objData.website,
      image_url: uploadFile,
      image_name: fileName,
    };
    commit(types.RESPONSE_FIREBASE_DATA, paramData);
    const {
      // eslint-disable-next-line no-unused-vars
      data: { data },
    } = await axios.post(url, paramData);
  },
};