import * as types from '@/store/types';

export default {
  [types.RESPONSE_FIREBASE_DATA](state, firebasedata) {
    state.firebasedata = firebasedata;
  },
};
