import * as types from '@/store/types';

export default {
  [types.GENERAL_AUDITION_LIST](state, auditionGeneralList) {
    state.auditionGeneralList = auditionGeneralList;
  },
};
