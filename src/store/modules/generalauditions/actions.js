import axios from 'axios';
import * as types from '@/store/types';

export default {
  async auditionsGeneralList({ commit }, objData) {
    try {
      const url = 'auditions/findby';
      const {
        data: { data },
      } = await axios.post(url, objData);
      commit(types.GENERAL_AUDITION_LIST, data);
    } catch (error) {
      commit(types.GENERAL_AUDITION_LIST, []);
    }
  },
};
