import * as types from '@/store/types';

export default {
  

  [types.SEARCH_QUERY_PARAM](state, searchQueryParam) {
    // eslint-disable-next-line prefer-template
    state.searchQueryParam = searchQueryParam;
  },

};
