import * as types from '@/store/types';
import 'firebase/storage';

export default {
  
 async searchAuditions({ commit }, { query }) {
    // eslint-disable-next-line no-multi-spaces
    // eslint-disable-next-line template-curly-spacing
    console.log(query);
    commit(types.SEARCH_QUERY_PARAM, query);
  },

};
