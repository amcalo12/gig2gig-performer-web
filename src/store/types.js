// Auth
export const SET_TOKEN = 'Set Token';

// Profile
export const FETCH_PROFILE_SUCCESS = 'Fetch Profile Successful';
export const FETCH_PROFILE_FAILURE = 'Fetch Profile Failed';

//Auditions
export const RESPONSE_FIREBASE_DATA = '';
export const GENERAL_AUDITION_LIST = '';
export const SEARCH_QUERY_PARAM = '';
