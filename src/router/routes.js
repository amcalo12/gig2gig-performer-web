import mapRoutes from './utils';

export default [

  /*
  |--------------------------------------------------------------------------
  | Guest Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register routes for guest access. These
  | routes require not being authenticated and are public
  | access.
  |
  */

  ...mapRoutes({ layout: 'authentication', onlyWhenLoggedOut: true }, [
    {
      path: '/login',
      alias: '/',
      name: 'login',
      component: () => import(/* webpackChunkName: 'login' */ '@/views/auth/Login'),
    },
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: 'register' */ '@/views/auth/Register'),
    },
    {
      path: '/password/email',
      name: 'password.email',
      component: () => import(/* webpackChunkName: 'email' */ '@/views/auth/password/Email'),
    },
  ]),

  /*
  |--------------------------------------------------------------------------
  | Protected Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register routes for private access. These
  | routes require authentication and are used for private
  | access.
  |
  */

  {
    path: '/auditions',
    name: 'auditions',
    component: () => import(/* webpackChunkName: 'auditions' */ '@/views/Auditions'),
  },
  {
    path: '/my/auditions',
    name: 'my.auditions',
    component: () => import(/* webpackChunkName: 'myauditions' */ '@/views/MyAuditions'),
  },
  {
    path: '/my/media',
    name: 'my.media',
    component: () => import(/* webpackChunkName: 'mymedia' */ '@/views/MyMedia'),
  },
  {
    path: '/my/settings',
    name: 'my.settings',
    component: () => import(/* webpackChunkName: 'mysettings' */ '@/views/Settings'),
  },
  {
    path: '/my/marketplace',
    name: 'my.marketplace',
    component: () => import(/* webpackChunkName: 'mysettings' */ '@/views/MyMarketplace'),
  },
  {
    path: '/my/profile',
    name: 'my.profile',
    component: () => import(/* webpackChunkName: 'mysettings' */ '@/views/MyProfile'),
  },
  {
    path: '/my/notifications',
    name: 'my.notifications',
    component: () => import(/* webpackChunkName: 'mysettings' */ '@/views/MyNotifications'),
  },
  {
    path: '/my/calendar',
    name: 'my.calendar',
    component: () => import(/* webpackChunkName: 'mysettings' */ '@/views/MyCalendar'),
  },
  {
    path: '/news-and-updates',
    name: 'news-and-updates',
    component: () => import(/* webpackChunkName: 'newsandupdates' */ '@/views/NewsAndUpdates'),
  },
  {
    path: '/news-and-updates/view',
    name: 'view.news',
    component: () => import(/* webpackChunkName: 'view.news' */ '@/views/ViewNews'),
  },
  {
    path: '/forum',
    name: 'forum',
    component: () => import(/* webpackChunkName: 'forum' */ '@/views/Forum'),
  },
  {
    path: '/forum/create',
    name: 'forum.create',
    component: () => import(/* webpackChunkName: 'forum.create' */ '@/views/CreateForum'),
  },
  {
    path: '/forum/view',
    name: 'forum.view',
    component: () => import(/* webpackChunkName: 'forum.view' */ '@/views/ViewForum'),
  },

  /*
  |--------------------------------------------------------------------------
  | Error Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register routes for error messages. These
  | routes are used when there is no match.
  |
  */

  ...mapRoutes({ layout: 'authentication', public: true }, [
    {
      path: '*',
      name: '404',
      component: () => import(/* webpackChunkName: '404' */ '@/views/errors/404'),
    },
  ]),

];
